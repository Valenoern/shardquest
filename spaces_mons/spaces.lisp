;; this is a second test of registering monsters from an extension
(defpackage :shardquest-spaces
	(:nicknames :M)
		;; due to the influence of glitch pokémon on this sphere I just can't not take the opportunity to use the symbol 'M
	(:use :common-lisp
		:shardquest-strain)
			;; 'strain 'resident
	(:local-nicknames
		(:ext :shardquest-plugin)
		(:item :shardquest-map)  ; item-related stuff lives in here currently
	)
	(:export
		;; species
		#:faun #:faun-baby #:faun-strong #:faun-keeper #:faun-hero
		#:killdeer #:killdeer-plover
		#:killdeer-parhelion #:killdeer-aphelion  ; tentative, may be moved to another strain
		
		;; portal items
		#:item-pear-common
		#:item-rock
		#:item-number #:item-number-natural #:item-number-complex
		
		;; other items
		#:item-pear-super #:item-pear-mythic
		#:item-number-imaginary #:item-number-infinite
))
(in-package :shardquest-spaces)


;;; species {{{

;; Fawna {{{

(defun faun ()
	(make-strain :name "Fawna")
)

(defun faun-baby ()  ; stage 1: fawna
	(make-strain :name "Fawna" :group 'faun)
)

(defun faun-strong ()  ; stage 2: guarden
	(make-strain :name "Guarden" :group 'faun
		:paths (list 'faun-baby))
)

(defun faun-keeper ()  ; stage 3: sentraur
	(make-strain :name "Sentraur" :group 'faun
		:paths (list 'faun-strong))
)

(defun faun-hero ()  ; stage 4: dheero
	(make-strain :name "Dheero" :group 'faun
		:paths (list 'faun-keeper))
)

;; }}}}

;;; weewit / parhero / aphellion {{{

(defun killdeer ()
	(make-strain :name "Weewit")
)

(defun killdeer-plover ()
	(make-strain :name "Weewit" :group 'killdeer)
)

(defun killdeer-parhelion ()
	(make-strain :name "Parhero" :group 'killdeer)
)

(defun killdeer-aphelion ()
	(make-strain :name "Aphellion" :group 'killdeer)
)

;;; }}}

(ext:register-species
	(list 'killdeer-plover 'faun-baby 'faun-strong 'faun-keeper 'faun-hero))

;;; }}}


;;; portal items {{{

(defun item-pear-common ()
	(item:make-item :name "Common Pear" :appeal 1))

(defun item-rock ()
	(item:make-item :name "Rock" :appeal -1))

;; Numbers are heptahedrons for capturing spaces_mons

(defun item-number ()
	(item:make-item :name "Number" :appeal 1 :recruit t
		))

(defun item-number-natural ()
	(item:make-item :name "Natural Number" :appeal 1 :recruit t
		))

(defun item-number-complex ()
	(item:make-item :name "Complex Number" :appeal 1 :recruit t
		))

(ext:register-portal-item
	(list 'item-pear-common 'item-rock 'item-number 'item-number-natural 'item-number-complex))

;;; }}}

;;; other items {{{

(defun item-pear-super ()
	(item:make-item :name "Super Pear" :appeal 1))

(defun item-pear-mythic ()
	(item:make-item :name "Mythic Pear" :appeal 1))


(defun item-number-imaginary ()
	(item:make-item :name "Imaginary Number" :appeal 1 :recruit t
		))

(defun item-number-infinite ()
	(item:make-item :name "Infinite Number" :appeal 1 :recruit t
		))

;;; }}}
