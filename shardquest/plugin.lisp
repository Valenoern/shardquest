;;; plugin.lisp - for adding monsters and items etc to shardquest
(defpackage :shardquest-plugin
	(:use :common-lisp)
	(:local-nicknames
		(:x :xylem)
			;; xylem-value
	)
	(:export
		;; species
		#:register-species #:get-species
		;; items
		#:register-portal-item #:get-portal-item
		
))
(in-package :shardquest-plugin)


;;; species {{{

(defun register-species (to-register) ; add species plugin {{{
	(unless (listp to-register)
		(setq to-register (list to-register)))
	
	(dolist (species to-register)
		(let (
			(species-id species)
			;; LATER: store actual species data
		)
		(x:xylem-value (list 'register-species species-id)
			:set-value species
			)
	))
	
	to-register
) ; }}}

(defun get-species (species-id  &key all) ; {{{
	(x:xylem-value-or-node 'register-species species-id :all all)
) ; }}}

;;; }}}

;;; items {{{

(defun register-portal-item (to-register) ; add species plugin {{{
	(unless (listp to-register)
		(setq to-register (list to-register))
		)
	
	(dolist (item to-register)
		(x:xylem-value (list 'register-portal-item item)
			:set-value item
			)
	)
	
	to-register
) ; }}}

(defun get-portal-item (item-id  &key all) ; {{{
	(x:xylem-value-or-node 'register-portal-item item-id :all all)
) ; }}}

;;; }}}
