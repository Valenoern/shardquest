;; load :package-file extension - see inside "grevillea" for more info
(asdf:load-system "grevillea")

(defsystem "shardquest"
	:author "Valenoern"
	:licence "AGPL"
		;; AGPL is necessary because there may eventually be a browser demo, albeit one that could run entirely offline.
	:description "zensekai-adjacent \"simple AR game simulator\""
	:homepage "https://codeberg.org/vxsh-suite/shardquest"
	
	:depends-on (
		;;"uiop"
		"cl-pcg"
		"grevillea"  ; library for bop etc - version 2022-11-10+a
		"xylem"      ; library for bop etc - version 2022-8-23+a
		;; these version IDs are found directly on the libraries' commit logs.
	)
	:components (
		;; simple scaffolding / structs only
		(:package-file "ns"  :packages (:shardquest-xylem))
		(:package-file "resident"  :packages (:shardquest-strain))
		(:package-file "plugin" :packages (:shardquest-plugin))
		
		;; LATER: may move to some basic library shared with zensekai?
		(:package-file "dice"  :packages (:spaces_dice))
		
		(:package-file "map"  :packages (:shardquest-map)
			:depends-on ("ns" "dice" "resident" "plugin"))
		
		(:package-file "main"  :packages (:shardquest)
			:depends-on ("dice" "map"))
))

;; Spaces_Mons "_" note:
;;   Note that while "_" isn't usually used in lisp symbols, here it's being considered a special unicode character that's necessary to spell "Spaces_Mons", or other phrases that start with "Spaces_"/"Space_" (but not ones that end with it). this exception doesn't apply to anything else.


#|
    * encounter resident ?
    * grid status
      * call items & monster forms by natural language name
      * move a default of 1 in (move-x) / (move-y)
    * organisation
      * :import-from some functions in map package into :shardquest
      * put xylem keys in :shardquest-xylem
      * create "spaces_mons" system to demo a second Sphere
      * shardquest.asd: specify packages with :package-file extension
|#

;; TODO:
;; - toss Numbers / Calling Card at monsters in incredibly simple die-roll encounters
;; - obtain monster in encounter and collect Shards

;; LATER:
;; - infinite use Calling Card awarded at start of game
;; - choose from four different Spaces_Mons at beginning... or none

;; insanely far-off but I've still gotta record it {{{
;; - try out CLIM or GTK, i haven't had success with either yet but graphical is the goal
;; - try out the seemingly insane possibility of binding the godot rendering engine to lisp
;; - random idea: use defgeneric on a variable containing a user interface to do the same game action in different CLIs/GUIs
;; }}}
